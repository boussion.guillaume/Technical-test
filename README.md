## Installation des dépendances
Dans un terminal à la racine du projet, lancer les commandes suivantes : 
```
Libs Laravel : "composer install"
Libs JS & CSS : "npm install && npm run dev"
```
## Création de la variable d'environnement
```
cp .env.example .env
```

## Génération de la clé d'application
```
php artisan key:generate
```

## Création de la base
Créer une base dont le nom est **"hellocse"**

## Construction des tables
Pour créer les tables : 
```
php artisan migrate
```

## Insertion des données de l'application
Pour insérer des données de base dans l'application, lancer 
```
php artisan db:seed --class=DatabaseInitialization
```

## Démarrage de l'application
```
php artisan serve
```

## Accès au back-office
Pour s'authentifier et accéder aux fonctions d'édition, ajout, suppression, se logger avec les identifiants suivants

Login : hellocse@gmail.com

MDP : admin123 (promis, mes mots de passe sont bien plus sécurisés)


