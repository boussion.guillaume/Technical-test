<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Models\Star;

class StarsController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['except' => ['homeIndex']]);
    }
    
    /**
     * Gets to the home page view whether user has credentiels or not
     *
     * @return \Illuminate\Http\Response
     */
    public function homeIndex()
    {
        $stars = Star::all();

        return view('index', [
            'stars' => $stars
        ]);
    }

    /**
     * Gets to the backoffice view if user has credentials.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stars = Star::all();

        return view('backoffice.index', [
            'stars' => $stars
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'firstName' => 'required|max:100',
            'lastName' => 'max:100',
            'description' => 'required|max:4000',
            'image' => 'required|mimes:jpg,png,jpeg|max:5048'
        ]);
        
        // Remplace l'ensemble des espaces pour éviter les erreurs d'url lors de l'accès à l'image + création de nom unique
        $newImageName = time() . '-' . str_replace(" ", "-", $request->name) . '.' . $request->image->extension();
        
        // Une fois l'image vérifiée & le nom modifié, déplacement dans le dossier adéquat
        $request->image->move(public_path('uploads/images'), $newImageName);

        $star = Star::create([
            'firstName' => $request->input('firstName'),
            'lastName' => $request->input('lastName'),
            'description' => $request->input('description'),
            'image_uri' => $newImageName
        ]);
        
        // Si les validations sont passées, le profil est créé et on retourne au back-office
        return redirect('/backoffice')
            ->with("success", true)
            ->with("msg", "Star successfully added !");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $star = Star::findOrFail($id);
        return view('backoffice.edit')->with('star', $star);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // L'image n'est pas obligatoire car possiblement pas mise à jour
        $request->validate([
            'firstName' => 'required|max:100',
            'lastName' => 'max:100',
            'description' => 'required|max:4000',
            'image' => 'mimes:jpg,png,jpeg|max:5048'
        ]);

        $star = Star::findOrFail($id);
        
        // Si l'image est mise à jour
        if(isset($request->image)){
           
            // Remplace l'ensemble des espaces pour éviter les erreurs d'url lors de l'accès à l'image + création de nom unique
            $imageName = time() . '-' . str_replace(" ", "-", $request->name) . '.' . $request->image->extension();
            
            // Une fois l'image vérifiée & le nom modifié, déplacement dans le dossier adéquat
            $request->image->move(public_path('uploads/images'), $imageName); 
        } else {
            // Sinon on passe l'URI de l'ancienne image
            $imageName = $star->image_uri;
        }
        

        $star->update([
            'firstName' => $request->input('firstName'),
            'lastName' => $request->input('lastName'),
            'description' => $request->input('description'),
            'image_uri' => $imageName
        ]);

        // Redirection à l'accueil du backoffice si tout est ok
        return redirect('/backoffice')
            ->with("success", true)
            ->with("msg", "Star successfully updated !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, int $id)
    {   
        $star = Star::where('id', $id)
            ->delete();
        
        // S'il s'agit d'un appel AJAX on retourne un JSON plutôt qu'une redirection
        if($request->ajax()){
            return response::json([
                'success' => true,
                'message' => "Star successfully deleted !"
            ]);
        }

        // Redirection à l'accueil du backoffice si tout est ok
        return redirect('/backoffice');
    }

    
}
