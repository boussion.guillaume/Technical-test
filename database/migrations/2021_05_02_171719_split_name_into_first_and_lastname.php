<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SplitNameIntoFirstAndLastname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stars', function (Blueprint $table) {
            // Erreur lors de la lecture du test technique, le couple "nom-prenom" était une seule colonne
            $table->string('lastName', 100)->nullable()->after('name');
            $table->renameColumn('name', 'firstName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stars', function (Blueprint $table) {
            $table->dropColumn('lastName');
            $table->renameColumn('firstName', 'name');
        });
    }
}
