<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseInitialization extends Seeder
{
    /**
     * Run the database seeds.
     * Data to initialize database and set some data to app.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('sql/initialize.sql');
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
