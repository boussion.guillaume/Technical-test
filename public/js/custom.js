$(document).ready(function() {
    //Horizontal Tab
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    // Child Tab
    $('#ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });

    //Vertical Tab
    $('#parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    // Requêtes AJAX pour les suppressions/modifications/ajouts
    $(".deleteElement").click( function(e) {
        
        // Passage du token CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();
      
        var id = $(this).data("id");
        var urlToSend = "/backoffice/" + id;
        
        $.ajax({
            type : "DELETE",
            url : urlToSend,
            success: function (data) {
                
                // En cas de succès, on supprime l'élément souhaité de l'interface
                $('#card-id-' + id).remove();
                showToast("success", data.message);
                console.log(data.message);
            },
            error: function (err) {
                console.log(err);
            }
        })
    })  

});

// Affiche un toast d'échec ou de succès après une modification
    function showToast(type, msg) {
        var toast = document.getElementById("toast");
        toast.innerHTML = msg;
        
        if(type === "success"){
            toast.className = "bg-green-500";
        } else {
            toast.className = "bg-red-500";
        }

        toast.className += " show";

        // On le retire après 3 secondes
        setTimeout(
            function(){
                toast.className = toast.className.replace("show", "");
            }, 3000
        )

    }