@extends('layouts.app')
@section('pageTitle', 'Create star profile')

@section('content')
    
    <div class="m-auto w-4/8 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">Create star</h1>
        </div>
    </div>

    <div class="flex justify-center pt-2">
        <form action="/backoffice" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="block">

                @error('firstName')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <input 
                    type="text"
                    class="block border shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400 @error('firstName') border-red-500 @enderror"
                    name="firstName"
                    value="{{ old('firstName') }}"
                    placeholder="Star first name..">

                @error('lastName')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <input 
                    type="text"
                    class="block border shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400 @error('lastName') border-red-500 @enderror"
                    name="lastName"
                    value="{{ old('lastName') }}"
                    placeholder="Star last name..">
                
                
                @error('description')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <textarea 
                    type="text"
                    class="block border shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400  @error('description') border-red-500 @enderror"
                    rows="15"
                    name="description"
                    placeholder="Description..">{{ old('description') }}</textarea>
                
                
                @error('image')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <input 
                    type="file"
                    class="block border shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400 @error('image') border-red-500 @enderror"
                    name="image">

                <button class="block mb-10 w-80 py-3 text-white hellocse-btn hellocse-bg-validate uppercase">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection