@extends('layouts.app')
@section('pageTitle', 'Edit star profile')

@section('content')
    
    <div class="m-auto w-4/8 pt-24 pb-15">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">Edit star</h1>
        </div>    
    
        <img src="{{ asset('uploads/images/' . $star->image_uri)}}" class="object-center mx-auto mt-10 w-36">
    </div>

    <div class="flex justify-center items-center pt-2">
        
        <form action="/backoffice/{{ $star->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="block">

                @error('firstName')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <input 
                    type="text"
                    class="block border shadow-5xl mb-10 p-2 w-96 placeholder-gray-400 @error('firstName') border-red-500 @enderror"
                    name="firstName"
                    value="{{ $star->firstName }}"
                    placeholder="Star first name.." required>


                @error('lastName')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <input 
                    type="text"
                    class="block border shadow-5xl mb-10 p-2 w-96 placeholder-gray-400 @error('lastName') border-red-500 @enderror"
                    name="lastName"
                    value="{{ $star->lastName }}"
                    placeholder="Star last name..">
                
                
                @error('description')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <textarea 
                    type="text"
                    class="block border shadow-5xl mb-10 p-2 w-96 placeholder-gray-400  @error('description') border-red-500 @enderror"
                    rows="15"
                    name="description"
                    placeholder="Description..">{{ $star->description }}</textarea>
                
                
                @error('image')
                    <p class="text-red-500 text-md mb-2">
                        {{ $message }}
                    </p>
                @enderror
                <input 
                    type="file"
                    class="block border shadow-5xl mb-10 p-2 w-96 placeholder-gray-400 @error('image') border-red-500 @enderror"
                    name="image">

                <button class="block mb-10 w-96 py-3 text-white hellocse-btn hellocse-bg-validate uppercase">
                    Update
                </button>
            </div>
        </form>
    </div>
@endsection