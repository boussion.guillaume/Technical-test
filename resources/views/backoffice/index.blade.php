@extends('layouts.app')
@section('pageTitle', 'Backoffice')

@section('content')
    <div class="m-auto w-4/5 pb-24 pt-15">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">
                Profile administration
            </h1>
        </div>
    

        <div class="pt-10 flex md:content-start sm:content-center">
            <a 
                href="backoffice/create" 
                class="border-b-2 py-3 px-7 border text-white hellocse-btn hellocse-bg-primary uppercase">
                Add a new star &rarr;
            </a>
        </div>

        <div class=" w-full py-10">
            
            <!-- S'il y a des données -->
            @if (count($stars) > 0)

            <div class="flex flex-wrap -m-3"> 
  
                @foreach ($stars as $star)
                <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/4 flex flex-col p-3" id="card-id-{{$star->id}}">
                  <div class="bg-white rounded-lg shadow-lg overflow-hidden flex-1 flex flex-col">
                    
                    <div class="bg-cover h-48" style="background-image: url({{ asset('uploads/images/' . $star->image_uri)}}); background-position: center;"></div>
                    
                    <div class="p-4 flex-1 flex flex-col">

                      <h3 class="mb-4 text-2xl">
                          <strong>{{ $star->firstName . ' ' . $star->lastName ?? $star->lastName }}</strong>
                        </h3>

                      <div class="mb-4 text-grey-darker text-sm flex-1">

                        <!-- Aperçu de la description dans la limite de 400 caractères + conservation de la mise en page -->
                        <p>{!! \Illuminate\Support\Str::limit(nl2br(e($star->description)), 400, '...') !!}</p>
                      </div>
                      
                      <div class="flex flex-row">
                            <a href="backoffice/{{ $star->id }}/edit" class="border-b-2 px-8 py-2 border text-white hellocse-btn hellocse-bg-warning uppercase">Edit</a>

                            <form action="/backoffice/{{ $star->id }}" method="POST">
                              @csrf
                              @method('delete')
                              <button data-id="{{ $star->id }}" type="submit" class="border-b-2 ml-2 px-8 py-2 border text-white hellocse-btn hellocse-bg-danger-outline uppercase deleteElement">Delete</a>
                            </form>
                      </div>
                      
                    </div>
                  </div>  
                </div>
                @endforeach
            </div>               
            @else
                <p><strong>No profile yet</strong></p>        
            @endif
        </div>
    </div>

    @if ($success = Session::get('success'))
      <script>
        $(document).ready(function () {
          showToast("success", "{{ Session::get('msg') }}");
        })
      </script>       
    @endif
              
    
    <!-- Toast de succès ou d'échec après une action de suppression/modification effectuée -->
    <div id="toast"></div>
@endsection