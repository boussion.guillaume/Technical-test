@extends('layouts.app')
@section('pageTitle', 'Profile browser')

@section('content')
    <div class="m-auto w-5/6 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">
                Profile Browser
            </h1>
        </div>

        <div class="lg:w-5/6 w-full py-10">
            
            <!-- If no data in table -->
            @if (count($stars) > 0)
            
                <div class="m-auto">

                    <div id="parentVerticalTab">
                        <ul class="resp-tabs-list hor_1">
                            @foreach ($stars as $star)
                                <li>{{ $star->firstName . ' ' . $star->lastName ?? $star->lastName }}</li>
                            @endforeach
                        </ul>
                        <div class="resp-tabs-container hor_1">
                            @foreach ($stars as $star)
                                <div>
                                    <div class="img-container">
                                        <img src="{{ asset('uploads/images/' . $star->image_uri) }}" class="img-thumbnail">
                                    </div>
                                    <h1 class="star-name mb-4">
                                        <strong>{{ $star->firstName . ' ' . $star->lastName ?? $star->lastName }}</strong>
                                    </h1>
                                    <p class="star-content">{!! nl2br(e($star->description))!!}</p>                            
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>

                
        @else
            <p><strong>No profile yet</strong></p>        
        @endif
        </div>
    </div>
@endsection