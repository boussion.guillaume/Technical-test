<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('pageTitle') </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Scripts for home screen, switching from tabs to accordion in responsive mode -->
    <script src="{{ asset('js/jquery-1.9.1.min.js') }}" ></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/easyResponsiveTabs.js') }}" defer></script>
    
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class=" h-screen antialiased leading-none font-sans">
    <div id="app">
        <header class="bg-hellocse py-6">
            <div class="container mx-auto flex justify-between items-center px-6">
                
                <div class="flex justify-between items-center">

                    <a href="{{ url('/') }}" class="text-lg font-semibold text-gray-100 no-underline">
                        <img src="{{ asset('images/navbar_logo.png') }}" alt="Logo" >
                    </a>

                    <!-- Only for logged in users -->
                    @if (Auth::user())
                        <a href="{{ url('/backoffice') }}" class="text-lg text-gray-100 no-underline ml-8 text-lg">
                            Backoffice
                        </a>    
                    @endif                    
                
                
                </div>
                <nav class="space-x-4 text-gray-300 text-sm sm:text-base">
                    @guest
                        <a class="no-underline hover:underline" href="{{ route('login') }}">{{ __('Login') }}</a>
                        @if (Route::has('register'))
                            <a class="no-underline hover:underline" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else
                        <span>{{ Auth::user()->name }}</span>

                        <a href="{{ route('logout') }}"
                           class="no-underline hover:underline"
                           onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                            {{ csrf_field() }}
                        </form>
                    @endguest
                </nav>
            </div>
        </header>

        @yield('content')
    </div>
</body>
</html>




