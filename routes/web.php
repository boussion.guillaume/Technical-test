<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StarsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Back-office de la liste des stars
Route::resource('/backoffice', StarsController::class);

// Gestion de l'authentification
Auth::routes();

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

// HomePage with stars displayed
Route::get('/', [\App\Http\Controllers\StarsController::class, 'homeIndex'])->name('welcome');

